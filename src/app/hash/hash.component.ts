import { Component, OnInit } from '@angular/core';
import { Model } from './model';

@Component({
  selector: 'app-hash',
  templateUrl: './hash.component.html',
  styleUrls: ['./hash.component.scss']
})
export class HashComponent implements OnInit {
    
  
  lists: Array<Model>;
    constructor(){
        this.lists = [];
    }

    addList(name){
        let list = new Model(name);
        this.lists.push(list);
    }

    removeList(list){
        let index = this.lists.indexOf(list);
        this.lists.splice(index,1);
    }

  ngOnInit() {
  }

}
